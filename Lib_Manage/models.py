from django.db import models

# Create your models here.
# from django.core.validators import MaxValueValidator, MinValueValidator

class Library(models.Model):
    lib_name = models.CharField(max_length = 255)

    def __unicode__(self):
       return str(self.lib_name)


class Plane(models.Model):
    plane_name = models.CharField(max_length = 255)

    def __unicode__(self):
       return str(self.plane_name)

class Book(models.Model):
    book_name = models.CharField(max_length = 255)
    library = models.ManyToManyField(Library)
    author = models.CharField(max_length = 255)
    subject = models.CharField(max_length = 255)
    book_availability = models.BooleanField(default = True)
    def __unicode__(self):
        return str(self.book_name) + ' ' + str(self.author) + ' ' + str(self.subject)


class User(models.Model):
    user_name = models.CharField(max_length = 255, unique = True)
    plane = models.ForeignKey(Plane)
    active = models.BooleanField(default = True)
    sub_yr = models.IntegerField(default = 0)
    no_of_books = models.IntegerField(default=0)
    def __unicode__(self):
       return str(self.user_name)

class BookRegister(models.Model):
    user = models.ForeignKey(User)
    book = models.ForeignKey(Book)
    issued_date = models.DateTimeField(auto_now_add=True)
