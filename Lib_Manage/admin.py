from django.contrib import admin

# Register your models here.
from models import Library, Plane, User, Book, BookRegister
# Register your models here.

admin.site.register(Library)
admin.site.register(Plane)
admin.site.register(User)
admin.site.register(Book)
admin.site.register(BookRegister)
