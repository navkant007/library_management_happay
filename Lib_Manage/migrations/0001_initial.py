# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('book_name', models.CharField(max_length=255)),
                ('author', models.CharField(max_length=255)),
                ('subject', models.CharField(max_length=255)),
                ('book_availability', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='BookRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('issued_date', models.DateTimeField(auto_now_add=True)),
                ('book', models.ForeignKey(to='Lib_Manage.Book')),
            ],
        ),
        migrations.CreateModel(
            name='Library',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lib_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Plane',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plane_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_name', models.CharField(unique=True, max_length=255)),
                ('active', models.BooleanField(default=True)),
                ('sub_yr', models.IntegerField(default=0)),
                ('no_of_books', models.IntegerField(default=0)),
                ('plane', models.ForeignKey(to='Lib_Manage.Plane')),
            ],
        ),
        migrations.AddField(
            model_name='bookregister',
            name='user',
            field=models.ForeignKey(to='Lib_Manage.User'),
        ),
        migrations.AddField(
            model_name='book',
            name='library',
            field=models.ManyToManyField(to='Lib_Manage.Library'),
        ),
    ]
