from django.shortcuts import render
from django.views.generic import View
from Lib_Manage.models import Library, Plane, Book, User, BookRegister
from django.http import HttpResponse

from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
# Create your views here.
class UserProfile(View):
    def get(self, request , user_name):
        answer = []
        user_details = User.objects.filter(user_name = user_name)
        for i in user_details:
            ans = {}
            ans['user_name'] = i.user_name
            ans['plane'] = i.plane.plane_name
            ans['active'] = i.active
            ans['sub_yr']= i.sub_yr
            ans['no_of_books_issued'] = i.no_of_books
            answer.append(ans)
        return HttpResponse(answer, status = 200)


class AddBook(View):
    def post(self, request):
        body = request.POST
        book_name1 = body['book_name']
        library1 = body['library']
        author1 = body['author']
        subject1 = body['subject']
        book_availability1 = body['book_availability']
        data = Library(lib_name = library1)
        data.save()
        book_instance = Book(book_name = book_name1 , author = author1 , subject = subject1 , book_availability = book_availability1)
        book_instance.save()
        book_instance.library.add(data)
        return HttpResponse("Successs", status = 200)


class CheckBookAvailability(View):
    def get(self, request, name):
        obj = Book.objects.all()
        ans = []
        obj1 = Book.objects.filter(book_name = name)
        for i in obj1:
            if i.book_availability == True:
                ans.append("Book named " + name + " is available")
            else:
                return HttpResponse("Not Available", status = 200)
        obj2 = Book.objects.filter(author = name)
        for i in obj2:
            if i.book_availability == True:
                ans.append("Author named " + name + " is available")
            else:
                return HttpResponse("Not Available", status = 200)
        obj3 = Book.objects.filter(subject = name)
        for i in obj3:
            if i.book_availability == True:
                ans.append("Subject named " + name + " is available")
            else:
                return HttpResponse("Not Available", status = 200)
        return HttpResponse(ans , status = 200)




class AddUser(View):
    def post(self, request):
        body = request.POST
        user_name1 = body['user_name']
        plane_name1 = body['plane_name']
        active1 = body['active']
        sub_yr1 = body['sub_yr']
        no_of_books1 = body['no_of_books']
        data = Plane(plane_name=plane_name1)
        data.save()
        navi = User(user_name = user_name1 ,plane=data, active = active1 , sub_yr = sub_yr1, no_of_books = no_of_books1)
        navi.save()
        return HttpResponse("User Added", status = 200)


class CalculatePanlty(View):
    def get(self, request, user_name):
        print(user_name)
        obj = User.objects.filter(user_name = user_name)
        print(obj)

        for o in obj:
            print(o.id)
        issued_date = datetime.now()
        bookobj= BookRegister.objects.filter(user_id = o.id)
        for i in bookobj:
            issued_date=i.issued_date
        today_date = datetime.now()
        print(issued_date)
        print(today_date)
        #if (today_date-book_issued_time) >= 15
        book_spent_time = today_date.date()- issued_date.date()
        print(book_spent_time)
        if book_spent_time > timedelta(days=15):
            exrta_time_taken = book_spent_time - timedelta(days=15)
            return HttpResponse("You have to pay for keeping the book " + exrta_time_taken + "days ")
        elif book_spent_time > timedelta(days=30):
            instance.active = False
            return HttpResponse("Subscription Cancel")
        else:
            return HttpResponse("You have the book only for " + str(book_spent_time) + "days! No subscription cancel")
