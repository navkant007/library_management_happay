Project Title - Library Management System
This project is created to deals with Library Management.It is created in python using Django Framework and tested On PostgreSQL Database.

Prereqiusite:
-Python 2.7
-Django 1.8
-Postman
-PostgreSQL Database

Functionalities:
User can add new books to the Library by using POSTMAN.
User can Check book availability in the library based on the book author/name/subject
User can Calculate penalty if the user has not returned the book within specified timeline.
User Deactivate the user (If the subscription has expired / if the user has not returned the book within 1 month.)
User can Retrieve the details of the user
User can Add new user to the system

API’s Implemented:
-GET /uerdetail/<user_name>  http://127.0.0.1:8000/userdetail<user_name>
-POST /addbook/  http://127.0.0.1:8000/addbook/  .  (You need to give book details in the body)
-GET /searchbook/  http://127.0.0.1:8000/searchbook/name  . (It can any name book_name, author_name, subject_name) 
-POST /adduser/.  http://127.0.0.1:8000/adduser/ . (You need to give user details in the body)
-GET /panalty/<user_name>. http://127.0.0.1:8000/panalty/<user_name>  . (In this query we have both query to calculate panalty and if he has book for more than 30 days then the subscription will be cancel

Running the Project:
To run the code just open terminal and go to the directory where the code folder is there . 
Run server by using command [python manage.py runserver]
There are certain urls provided to run certain urls as mentioned above.
For PostgreSQL use port 5434