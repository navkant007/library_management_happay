from django.conf.urls import include, url
from django.contrib import admin
from Lib_Manage.views import UserProfile, AddBook, AddUser
from Lib_Manage.views import CheckBookAvailability, CalculatePanlty

urlpatterns = [
    # Examples:
    # url(r'^$', 'LibraryManagement.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', admin.site.urls),
    url(r'^userdetails/(?P<user_name>\D+)/', UserProfile.as_view(),name = 's'),
    url(r'^addbook/', AddBook.as_view(),name = 's'),
    url(r'^searchbook/(?P<name>\D+)/', CheckBookAvailability.as_view(),name = 's'),
    url(r'^adduser/', AddUser.as_view(),name = 's'),
    url(r'^panalty/(?P<user_name>\D+)', CalculatePanlty.as_view(),name = 's'),
]
